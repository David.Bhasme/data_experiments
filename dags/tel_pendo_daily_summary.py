import sys
import os

import airflow
import json
import logging

from datetime import date, datetime, timedelta
from airflow import DAG

from airflow.models import Variable

from airflow.contrib.kubernetes.pod import Resources
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator

from airflow.contrib.operators.bigquery_operator import BigQueryOperator
from airflow.contrib.operators.bigquery_table_delete_operator import BigQueryTableDeleteOperator
from airflow.contrib.operators.gcs_to_bq import GoogleCloudStorageToBigQueryOperator


log = logging.getLogger(__name__)

def extract_pendo_dag(
            dag_id,
            schedule,
            query_json,
            default_args):

    BQ_CONN_ID = "bigquery_default"
    GCP_CONN_ID = "google_cloud_default"

    dag_config = Variable.get("gcp_scpdi_experiments", deserialize_json=True)
    GCP_BUCKET = dag_config["gcp_bucket"]
    BQ_DATASET_ID = dag_config["bigquery_dataset_id"]
    GCP_PROJECT_ID = dag_config["gcp_project_id"]

    PENDO_API_INTEGRATION_KEY = Variable.get("pendo_integration")

    query_json_file_base = os.path.basename(query_json)
    query_name = os.path.splitext(query_json_file_base)[0]
    source = query_name.split("_")[0]

    gcs_temp_json = "temp/pendo/" + source + "/" + query_name

    bq_temp_table_id = f"{GCP_PROJECT_ID}.temp_{BQ_DATASET_ID}.raw_{query_name}"
    bq_table_id = f"{GCP_PROJECT_ID}.{BQ_DATASET_ID}.raw_{query_name}"

    extract_pendo_cmd = f"""
        git config --global credential.'https://source.developers.google.com'.helper gcloud.sh &&
        git clone -b k8 --single-branch https://source.developers.google.com/p/scp-api-1145/r/scpdi_analytics.git --depth 1 &&
        cd scpdi_analytics/extract/pendo/src/ &&
        python execute.py execute --source {source} --query_json {query_json}"""

    unnest_temp_sql = f"""
        SELECT
            date,
            featureEvent.accountId AS accountId,
            featureEvent.browser AS browser,
            featureEvent.featureId AS featureId,
            featureEvent.minutes AS minutes,
            featureEvent.numClicks AS numClicks,
            featureEvent.visitorId AS visitorId
        FROM
            {bq_temp_table_id} ,
            UNNEST (featureEvents) AS

    """
    # Set the resources for the task pods
    pod_resources = Resources(request_memory="1Gi", request_cpu="500m")

    # SCPDI default settings for all DAGs
    scpdi_defaults = dict(
        get_logs=True,
        image_pull_policy="Always",
        namespace="default",
        resources=pod_resources,
        affinity={
            'nodeAffinity': {
                'requiredDuringSchedulingIgnoredDuringExecution': {
                    'nodeSelectorTerms': [{
                        'matchExpressions': [{
                            'key': 'cloud.google.com/gke-nodepool',
                            'operator': 'In',
                            'values': [
                                'air-pod-1']}]}]}}},
        is_delete_operator_pod=True
    )

    dag = DAG(
            dag_id,
            schedule_interval=schedule,
            default_args=default_args)

    with dag:
        t1 = KubernetesPodOperator(
            **scpdi_defaults,
            image="us.gcr.io/scp-api-1145/data-image:latest",
            task_id=f"{dag_id}_api_to_gcs_task",
            name=f"pendo-api-to-gcs",
            cmds=["/bin/bash", "-c"],
            env_vars={'PENDO_API_INTEGRATION_KEY': PENDO_API_INTEGRATION_KEY},
            arguments=[(extract_pendo_cmd + " --start_date_str {{ yesterday_ds }}")],
            dag=dag
        )

        t1

    return dag

query_jsons = ["../resources/requests/aggregation/featureEvents_daily_summary.json"]

# for root, dirs, files in os.walk("../resources/requests/aggregation/"):
#     for filename in files:
#         query_jsons.append(os.path.join(root,filename))

# create DAG for each query
for query_json in query_jsons:
    file_base = os.path.basename(query_json)
    query_name = os.path.splitext(file_base)[0]
    query_name = query_name.replace("-","_")

    dag_id = f"pendo_extract_{query_name}"
    default_args = {
        "owner": "scpdi",
        "catchup": True,
        "depends_on_past": False,
        "start_date": datetime(2019, 6, 4),
        "end_date": datetime(2019, 6, 8)}

    schedule = "30 5 * * *"

    globals()[dag_id] = extract_pendo_dag(
                            dag_id=dag_id,
                            schedule=schedule,
                            query_json=query_json,
                            default_args=default_args)
