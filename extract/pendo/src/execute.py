"""
1. Execute a pendo request for a query, source, and single date.
2. Save results to a local json file.
    TODO: if the result set is too large, pendo times out.
    Instead, breakdown the request into multiple requests,
    divided by a given hour range
3. Upload json to gcs folder
4. Cleanup local json file
"""
import os
import sys
import json
import logging

import pytz
import re

from datetime import date, datetime, time, timedelta
from fire import Fire

from gcp_utils import upload_to_gcs
from api import PendoAPI, PendoQueryBuilder

ADP_HQ_TZ = "US/Eastern"
RESOURCE_PATH = "../resources/requests/aggregation/"
EVENTS_RESOURCE_FILE_BASE = "_daily_summary"
DEFAULT_RESOURCE_FILE_BASE = "_detail"


def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)+1):
        yield start_date + timedelta(n)


def clean_keys(response_dict):
    translate = {}
    for k, v in response_dict.items():
        if isinstance(v, dict):
                clean_keys(v)
        elif isinstance(v, list):
            for l in v:
                if isinstance(l, dict):
                    clean_keys(l)

        new_key = k
        if (re.match("[a-z,A-Z,\_]", k[0:1]) is None):
            new_key = f"_{k}"

        translate[k] = new_key

    for old, new in translate.items():
        response_dict[new] = response_dict.pop(old)


def beginning_of_day_local_tz(date: datetime, tz="US/Eastern") -> datetime:
    local_tz = pytz.timezone(tz)
    beginning_of_day = datetime.combine(date.date(), time())
    beginning_of_day_local = local_tz.localize(beginning_of_day)

    return beginning_of_day_local


def execute(
    source: str,
    start_date_str: str=None,
    end_date_str: str=None,
    query_json: str = None
        ) -> None:

    RESOURCE_FILE_BASE = EVENTS_RESOURCE_FILE_BASE if "Events" in source else DEFAULT_RESOURCE_FILE_BASE

    if start_date_str is None:
        start_date_str = datetime.strftime(datetime.now() - timedelta(1), '%Y-%m-%d')

    if end_date_str is None:
        end_date_str = start_date_str

    if query_json is None:
        query_json = f'{RESOURCE_PATH}{source}{RESOURCE_FILE_BASE}.json'
        print(query_json)

    start_date = datetime.strptime(start_date_str, '%Y-%m-%d')
    end_date = datetime.strptime(end_date_str, '%Y-%m-%d')

    for query_date in daterange(start_date, end_date):
        json_out = extract_pendo_to_json(
            query_date=query_date,
            source=source,
            query_json=query_json)

        gcs_blob_id = upload_to_gcs(
            source_file_name=json_out,
            bucket_name = "scpdi_experiments",
            bucket_folder=f"data/pendo/{source}")

        cleanup(filename=json_out)


def extract_date() -> str:
    today_date = datetime.now()
    return datetime.strftime(today_date, '%Y-%m-%d')


def cleanup(filename: str) -> None:
    try:
        os.remove(filename)
        logging.info(f"Removed {filename}")
    except Exception as e:
        logging.error(f"Error attempting to delete {filename}")
        logging.error(e)


def extract_pendo_to_json(
    query_date: datetime,
    query_json: str,
    source: str
        ) -> str:

    first_date = str(beginning_of_day_local_tz(query_date, ADP_HQ_TZ).timestamp()*1000)
    query_file_base = os.path.basename(query_json)
    print(query_file_base)

    with open(query_json) as json_file:
        logging.info(f"Fetching query from '{json_file.name}'")
        json_data = json.load(json_file)

    query = PendoQueryBuilder(json_data)

    if "Events" in source:
        query.update_timeSeries(first=f"{first_date}")

    pendo = PendoAPI()
    r = pendo.get_aggregation_json(query=query.query_dict)

    clean_keys(r)

    r[source] = r.pop('results')

    if "Events" in source:
        query_result_timestamp = r['startTime']


        if float(first_date) != float(query_result_timestamp):
            logging.warning(f"query timestamp ({first_date}) does not match\
                 query result timestamp ({query_result_timestamp})")

        logging.info(f"Saving results for {query_date}")

    yyyymmdd = f"_{date.strftime(query_date, '%Y%m%d')}"
    r['date'] = query_date.strftime('%Y-%m-%d')
    r['extract_date'] = extract_date()

    query_file_base_no_ext = os.path.splitext(query_file_base)[0]
    outfile = f"{query_file_base_no_ext}{yyyymmdd}.json"

    with open(f'{outfile}', 'w') as json_out:
        json.dump(r, json_out)

    logging.info(f"Created temp {outfile}")

    return outfile


if __name__ == "__main__":
    logging.basicConfig(stream=sys.stdout, level=20)
    Fire({"execute": execute})
    logging.info("Complete.")
