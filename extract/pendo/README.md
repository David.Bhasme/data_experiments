# Pendo Extractor
This extractor queries the Pendo API for the following data sets:

Aggregation Data - https://developers.pendo.io/docs/?python#aggregation

Feature Events
Guide Events
Page Events
Poll Events
Track Events
